const webpack = require('webpack')
const { join } = require('path')

module.exports = {
  head: {
    titleTemplate: 'Peladator',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Meta description' }
    ],
    link: [
      { rel: 'stylesheet', href: 'http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css' },
      { rel: 'stylesheet', href: 'http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons', type: 'text/css' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700', type: 'text/css' }
    ]
  },
  build: {
    vendor: ['axios', 'vue-i18n', 'jquery', 'bootstrap'],
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
      })
    ]
  },
  router: {
    middleware: 'i18n'
  },
  plugins: [
    '~plugins/axios',
    '~plugins/vendor/bootstrap.js',
    { src: '~plugins/i18n.js', injectAs: 'i18n' }
  ],
  css: [
    'bootstrap/dist/css/bootstrap.css',
    join(__dirname, 'css/vendor/material-kit.css')
  ],
  env: {
    baseUrl: process.env.BASE_URL || 'http://localhost:4000/api'
  },
  loading: {
    color: '#7b1fa2'
  }
}
