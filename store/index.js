export const state = {
  locales: ['en', 'pt-br'],
  locale: 'en',
  user: null
}

export const mutations = {
  SET_LANG (state, locale) {
    if (state.locales.includes(locale)) {
      state.locale = locale
    }
  },
  SET_USER (state, user) {
    state.user = user
  }
}

export const actions = {
  login ({ commit }, { username, password }) {
    return commit('SET_USER', {username: username})
  }
}
