export default function ({req, store, i18n}) {
  const locale = req.headers['accept-language'] || 'en'
  // Set locale
  store.commit('SET_LANG', locale)
  i18n.locale = store.state.locale
}
